import React from "react";
import Calculator from "./components/Calculator";

const CalculatorApp = () => {
  return (
    <div className="bg-secondary d-flex justify-content-center align-items-top vh-100">
      <Calculator />
    </div>
  );
};

export default CalculatorApp;
