import React, { useState } from "react";
import { FormGroup, Label, Input } from "reactstrap";

const Calculator = props => {
  return (
    <FormGroup>
      <Label>{props.label}</Label>
      <Input
        style={{ width: "75%" }}
        placeholder={props.placeholder}
        defaultValue={props.defaultValue}
        onChange={props.onChange}
        type="number"
      ></Input>
    </FormGroup>
  );
};

export default Calculator;
