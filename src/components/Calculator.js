import React, { useState } from "react";
import CalculatorInput from "./CalculatorInput";

const Calculator = () => {
  const [value1, setValue1] = useState(0);

  const handleValue1 = e => {
    setValue1(e.target.value);
  };

  return (
    <div style={{ width: "70%" }} className="my-5">
      <CalculatorInput
        label={"Calculator"}
        placeholder={"Amount to convert"}
        onChange={handleValue1}
      />
    </div>
  );
};

export default Calculator;
